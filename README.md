# Flyway Lambda

This repo provides a layer for AWS Lambda containing Flyway, simplifying the process of running
migrations against an RDS database inside a VPC.

## Quickstart
This quick start assumes you have [serverless](https://github.com/serverless/serverless) installed
and have a basic understanding of how to use it with AWS.

1. Create a new project from the example in this repo:
```
mkdir migrations
cd migrations
curl -O https://gitlab.com/ourstreets/flyway-lambda/-/raw/master/example/serverless.yml
```
2. Update the region and layer arn(see table below) in `serverless.yml`
3. Add your migrations to the root  your new project. (eg: create `VOO1__initial.sql`)
4. Update `FLYWAY_URL`, `FLYWAY_USER`, and `FLYWAY_PASSWORD` in `serverless.yml`
5. Deploy:
```
sls deploy
```
6. Run your migrations:
```
sls invoke -f migrate
```

## Layer ARNs
| Region     | ARN                                                    |
| ---------- | ------------------------------------------------------ |
| us-east-2  | `arn:aws:lambda:us-east-2:044220569105:layer:flyway:4` |

## Deploy the layer yourself
If you want to deploy the layer to a different region, or just want to deploy it yourself for other
reasons, you will need Java, Maven, & Serverless installed. Then run the following commands to
clone the repo, build the JAR, and deploy to AWS. Update the region in the last command as
necessary.

```
git clone https://gitlab.com/ourstreets/flyway-lambda
mvn clean install
sls deploy --region us-east-1
```

**NOTE:** because this repo is used to publish the layers in the table above, the layer deployed is
publically usable by all AWS accounts. To change this, edit the `allowedAccounts` option in
`serverless.yml`
